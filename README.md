# X-Script

An attempt at making a tool, which would let one connect multiple programming languages and communicate between them (C#, Python).

## Installation guide:
```bash
# Change directory into where you want your x-script directory to be created
cd D:\

# Run setup.bat
.\setup.bat

# Run the app
npm start
```