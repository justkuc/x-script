﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Linq;
using System.IO;
using Xlang.SharpDetection;
using Newtonsoft.Json;
using System.Security.Cryptography;
using Xlang.SharpDetectionObjects;

namespace Xlang
{
    class Program
    {

        public static String OUT_DIRECTORY = "";
        public static String SRC_DIRECTORY = "";


        static void Main(string[] args)
        {
        
            if (args.Length !=2)
            {
                Console.WriteLine("Out and Src paths are required");
                return;
            }
           

            SRC_DIRECTORY = @args[0];
            OUT_DIRECTORY = @args[1];

            Console.WriteLine("Source directory: " + @SRC_DIRECTORY);
            Console.WriteLine("Output directory: " + @OUT_DIRECTORY);
          
            Console.WriteLine();
            Console.WriteLine("------------------Starting--------------------");
            //Extract JSON information
            Console.WriteLine("Generating JSON files...");
            extractJsonSharp();
            extractJsonPy();

            //Analyze Python calls inside sharp classes and alter jsons.
            Console.WriteLine("Analyzing python calls in C#...");
            analyzePythonCallsInCSharp();

            //Generate .h headers
            Console.WriteLine("Generating header files");
            extractHeaders(@OUT_DIRECTORY + @"\Sharp\");
            extractHeaders(@OUT_DIRECTORY + @"\Python\");
            
            Console.WriteLine("-------------------Finished-------------------");
        
    
    }

        static void analyzePythonCallsInCSharp()
        {
            var sharpFiles = Directory.GetFiles(@SRC_DIRECTORY + @"\Sharp\", "*.*", SearchOption.AllDirectories)
           .Where(s => s.EndsWith(".txt"));

            SharpAnalyzer sharpAnalyzer = new SharpAnalyzer();
            foreach (String path in sharpFiles)
            {
                sharpAnalyzer.inspectPythonCalls(path);
            }
        }

        static void extractJsonPy()
        {
            var pyFiles = Directory.GetFiles(@SRC_DIRECTORY + @"\Python\", "*.*", SearchOption.AllDirectories)
           .Where(s => s.EndsWith(".py"));

            PyAnalyzer pyAnalizer = new PyAnalyzer();
            foreach (String path in pyFiles)
            {
                pyAnalizer.PyClassAnalyzer(path);
            }
        }


        static void extractJsonSharp()
        {
            var sharpFiles = Directory.GetFiles(@SRC_DIRECTORY + @"\Sharp\", "*.*", SearchOption.AllDirectories)
            .Where(s => s.EndsWith(".txt"));

            SharpAnalyzer sharpAnalyzer = new SharpAnalyzer();
            foreach (String path in sharpFiles)
            {
                sharpAnalyzer.CSharpClassAnalyzer(path);
            }
        }

        static void extractHeaders(String p)
        {

            var sharpFiles = Directory.GetFiles(p, "*.*", SearchOption.AllDirectories)
            .Where(s => s.EndsWith(".json"));

            HeaderGenerator sharpHeaderGenerator = new HeaderGenerator();
            
            
            foreach (String path in sharpFiles)
            {
                sharpHeaderGenerator.GenerateHeader(path);
            }
        }



        static String compareFiles(String fileOne, String fileTwo)
        {
            var md5 = MD5.Create();
            
            var streamOne = File.OpenRead(fileOne);
            var streamTwo = File.OpenRead(fileTwo);
            
            var sumOne = Convert.ToBase64String(md5.ComputeHash(streamOne));
            var sumTwo = Convert.ToBase64String(md5.ComputeHash(streamTwo));

            return sumTwo.Equals(sumOne) ? "Files match" : "Files do not match";
        }

    }

}