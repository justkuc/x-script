#pragma once

#include <unordered_map>
#include <vector>

// Back-end interface
#include "Languages/BackendInterface.h"

/**
 * Class used to keep different language objects and is responsible for mapping
 * them between different languages
 */
class XLangRuntime
{
public:
    ~XLangRuntime();

    /**
     * Add a code source to the runtime
     */
    template<class T, typename... Args>
    void addSource(Args... args)
    {
        // Create the source object
        T* pSource = new T(std::forward<Args...>(args...));

        // Now check if the back-end for this source exists
        if (m_backends.find(pSource->getBackend()) == m_backends.end()) 
        {
            std::cout << "[Error] Source could not be added cause back-end " << pSource->getBackend() << " was not found!";
            delete pSource;
            return;
        }
        else 
        {
            // Add source
            m_sources.push_back(pSource);
        }
    }

    /**
     * Load a back-end of type T to the runtime, the back-end must inherit IBackend interface
     */
    template<class T>
    void loadBackend()
    {
        // Create back-end
        T* pBackend = new T();

        // Add to map
        m_backends[pBackend->getName()] = pBackend;
    }

    /**
     * Create a new object of type T
     */
    template<class T>
    T* newObject()
    {
        // Create object
        T* pObject = new T();

        // Now check if the back-end for this object exists
        if (m_backends.find(pObject->getBackend()) == m_backends.end())
        {
            std::cout << "[Error] Object could not be created cause back-end " << pObject->getBackend() << " was not found!";
            delete pObject;
            return nullptr;
        }
        else
        {
            // Add object
            m_backends[pObject->getBackend()]->newObject(pObject);
            return pObject;
        }
    }

    /**
     * Start the runtime and all its back-ends
     */
    void start();

    /**
     * Return IObject pointer whose instance id matches the one specified
     * nullptr if there was an error or an object doesn't exist with the instance id
     */
    IObject* getObject(const std::string& instanceID);
private:
    /**
     * Back-ends were loaded into the runtime
     */
    std::unordered_map<std::string, IBackend*> m_backends;

    /**
     * Sources managed by the runtime
     */
    std::vector<ISource*> m_sources;
};
