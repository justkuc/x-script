#pragma once

#include <vector>
#include <string>
#include <functional>

// Mono
#include <mono/jit/jit.h>
#include <mono/metadata/assembly.h>
#include <mono/metadata/mono-config.h>
#include <mono/metadata/attrdefs.h>
#include <mono/metadata/mono-gc.h>
#include <mono/metadata/threads.h>
#include <mono/metadata/tokentype.h>
#include <mono/metadata/debug-helpers.h>
#include <mono/utils/mono-logger.h>
#include <mono/metadata/mono-debug.h>

#include "Languages/BackendInterface.h"
#include "Utility/Memory.h"

class SharpBackend;
class ScriptObject;
class ScriptBuilder;

namespace TypeReflection
{
	/**
	* Helper class used to get type name
	*/
	template<typename T>
	inline const char* typeName(void) { return "unknown"; }

	template<>
	inline const char* typeName<int>(void) { return "int"; }

	template<>
	inline const char* typeName<int*>(void) { return "int&"; }

	template<>
	inline const char* typeName<std::string>(void) { return "string"; }

	template<>
	inline const char* typeName<MonoString>(void) { return "string"; }

	template<>
	inline const char* typeName<MonoString*>(void) { return "string"; }
}

/**
 * Helper class used to convert between C++ and C# types
 */
template<typename T>
class TypeConversion
{
public:
	TypeConversion(MonoObject* pObject)
		: pMonoValue(pObject), Value()
	{
		if (!pObject)
		{
			return;
		}

		if (mono_class_is_valuetype(mono_object_get_class(pObject)))
		{
			Value = *static_cast<T*>(mono_object_unbox(pObject));
		}
		else
		{
			Value = *reinterpret_cast<T*>(pObject);
		}
	}

	TypeConversion(const T& value)
		: Value(value), pMonoValue(reinterpret_cast<MonoObject*>(&Value))
	{}

	TypeConversion(T* value)
		: Value(*value), pMonoValue(reinterpret_cast<MonoObject*>(&Value))
	{}

	T Value;
	MonoObject* pMonoValue;
};

/**
 * Helper class used to convert between C++ and C# string
 */
template<>
class TypeConversion<std::string>
{
public:
	TypeConversion(MonoObject* pObject)
		: pMonoValue(mono_object_to_string(pObject, nullptr))
	{
		// Get string buffer
		char* pStr = mono_string_to_utf8(pMonoValue);

		// Create std::string
		Value = reinterpret_cast<std::string::value_type*>(pStr);

		// Free the memory of the raw array
		mono_free(pStr);
	}

	TypeConversion(const std::string& value)
		: Value(value), pMonoValue(mono_string_new_wrapper(Value.c_str()))
	{}

	TypeConversion(std::string* value)
		: Value(*value), pMonoValue(mono_string_new_wrapper(Value.c_str()))
	{}

	std::string Value;
	MonoString* pMonoValue;
};

/**
 * Struct containing exception info during a method invoke or any other operation
 */
struct Exception
{
	Exception();
	Exception(MonoObject* ex);

	std::string Message = "";
	std::string StackTrace = "";
};

/**
 * Generic field that can be used for any type of fields
 */
class GenericField
{
public:
	GenericField();
	GenericField(const std::string& name);
	GenericField(MonoClassField* pField, MonoObject* pObject);
	GenericField(MonoClassField* pField, MonoVTable* pVTable);

	virtual ~GenericField()
	{}

	const std::string& getName() const;

	void getValue(void** pValue);
	void setValue(void* pValue);
private:
	std::string m_name;
	bool m_static;
	MonoClassField* m_pField = nullptr;
	union _FieldInstance
	{
		MonoObject* pObject;
		MonoVTable* pVTable;
	} m_instance;

	friend ScriptBuilder;
};

/**
 * Generic method that can be used for any type of methods
 */
class GenericMethod
{
public:
	GenericMethod();
	GenericMethod(const std::string& signature);
	GenericMethod(MonoMethod* pMethod, MonoObject* pObject);

	virtual ~GenericMethod()
	{}

	const std::string& getSignature() const;

	void* invokeGeneric(void** params, Exception*& ex);
private:
	std::string m_signature;
	MonoMethod* m_pMethod = nullptr;
	MonoObject* m_pObject = nullptr;

	friend ScriptBuilder;
};

/**
 * Generic property that can be used for any type of properties
 */
class GenericProperty
{
public:
	GenericProperty();
	GenericProperty(const std::string& name);
	GenericProperty(MonoProperty* pProperty, MonoObject* pObject);

	virtual ~GenericProperty()
	{}

	const std::string& getName() const;

	void getValue(void** pValue, Exception*& ex);
	void setValue(void* pValue, Exception*& ex);
private:
	std::string m_name;
	MonoProperty* m_pProperty = nullptr;
	MonoObject* m_pObject = nullptr;

	friend ScriptBuilder;
};

/**
 * Specialized T type field
 */
template<typename T>
class Field : public GenericField
{
public:
	Field() {}
	Field(const std::string& name)
		: GenericField(name)
	{}

	Field(MonoClassField* pMethod, MonoObject* pObject)
		: GenericField(pMethod, pObject)
	{}

	Field(MonoClassField* pMethod, MonoVTable* pVTable)
		: GenericField(pMethod, pVTable)
	{}

	Memory::managed_ptr<T> get()
	{
		return getGeneric<T>();
	}

	void set(T* pValue)
	{
		setGeneric<T>(pValue);
	}

	Field<T>& operator=(T* pNewValue)
	{
		set(pNewValue);
		return *this;
	}

	Field<T>& operator=(T newValue)
	{
		set(&newValue);
		return *this;
	}
private:
	template<typename Type>
	void setGeneric(Type* pValue)
	{
		this->setValue(static_cast<void*>(TypeConversion<Type>(pValue).pMonoValue));
	}

	template<typename Type>
	Type* getGeneric()
	{
		void* pRet = {};
		this->getValue(&static_cast<void*>(pRet));
		return new Type(TypeConversion<Type>(static_cast<MonoObject*>(pRet)).Value);
	}
};

/**
 * Specialized Return method taking Args as parameters
 */
template<typename Ret, typename... Args>
class Method : public GenericMethod
{
public:
	Method(const std::string& signature)
		: GenericMethod(signature)
	{}

	Method(MonoMethod* pMethod, MonoObject* pObject)
		: GenericMethod(pMethod, pObject)
	{}

	std::tuple<Ret, Memory::managed_ptr<Exception>> invoke(Args&... params)
	{
		std::vector<void*> args = { toGenericParam(params)... };
		Exception* ex = nullptr;
		void* result = this->invokeGeneric(args.data(), ex);
		return { TypeConversion<Ret>(static_cast<MonoObject*>(result)).Value, ex };
	}

	std::tuple<Ret, Memory::managed_ptr<Exception>> operator()(Args&... params) { return invoke(std::forward<Args>(params)...); }

private:
	template<typename T>
	void* toGenericParam(T& param)
	{
		return static_cast<void*>(&param);
	}

	template<>
	void* toGenericParam<std::string>(std::string& param)
	{
		return this->convertString(param);
	}
};

/**
 * Specialized void method taking Args as parameters
 */
template<typename... Args>
class Method<void, Args...> : public GenericMethod
{
public:
	Method(const std::string& signature)
		: GenericMethod(signature)
	{}

	Method(MonoMethod* pMethod, MonoObject* pObject)
		: GenericMethod(pMethod, pObject)
	{}

	std::tuple<Memory::managed_ptr<Exception>> invoke(Args&&... params)
	{
		std::vector<void*> args = { toGenericParam(params)... };
		Exception* ex = nullptr;
		this->invokeGeneric(args.data(), ex);
		return { ex };
	}

	std::tuple<Memory::managed_ptr<Exception>> operator()(Args&&... params) { return invoke(std::forward<Args>(params)...); }

private:
	template<typename T>
	void* toGenericParam(T& param)
	{
		return TypeConversion<T>(param).pMonoValue;
	}
};

/**
 * Specialized T type property
 */
template<typename T>
class Property : public GenericProperty
{
public:
	Property() {}
	Property(const std::string& name)
		: GenericProperty(name)
	{}

	Property(MonoProperty* pProperty, MonoObject* pObject)
		: GenericProperty(pProperty, pObject)
	{}

	std::tuple<Memory::managed_ptr<T>, Memory::managed_ptr<Exception>> get()
	{
		void* pRet = {};
		Exception* ex = nullptr;
		this->getValue(&static_cast<void*>(pRet), ex);
		return { new T(TypeConversion<T>(static_cast<MonoObject*>(pRet)).Value), ex};
	}

	Memory::managed_ptr<Exception> set(T* pValue)
	{
		Exception* ex = nullptr;
		this->setValue(static_cast<void*>(TypeConversion<T>(pValue).pMonoValue), ex);
		return ex;
	}

	Property<T>& operator=(T* pNewValue)
	{
		set(pNewValue);
		return *this;
	}

	Property<T>& operator=(T newValue)
	{
		set(&newValue);
		return *this;
	}
};

/**
 * Class used to build script object interface
 */
class ScriptBuilder
{
	struct SignatureParams
	{
		bool ReferenceTypes;
		bool IncludeThis;
		bool Static;
	};
public:
	ScriptBuilder(SharpBackend* pInterop, ScriptObject* pObject);

	bool isValid() const
	{
		return m_valid;
	}

	/**
	 * Get field with the specified name
	 */
	template<typename T>
	Memory::reference<Field<T>> resolveField(const std::string& name)
	{
		Field<T>* field = new Field<T>(name);
		resolveField(static_cast<GenericField*>(field));
		return field;
	}

	/**
	 * Get method with the specified name
	 */
	template<typename Ret, typename... Args>
	Memory::reference<Method<Ret, Args...>> resolveMethod(const std::string& funcName)
	{
		std::string signature = resolveSignature<Args...>(funcName);
		Method<Ret, Args...>* method = new Method<Ret, Args...>(signature);
		resolveMethod(static_cast<GenericMethod*>(method));
		return method;
	}

	/**
	 * Get property with the specified name
	 */
	template<typename Type>
	Memory::reference<Property<Type>> resolveProperty(const std::string& name)
	{
		Property<Type>* prop = new Property<Type>(name);
		resolveProperty(static_cast<GenericProperty*>(prop));
		return prop;
	}

	/**
	 * Links C++ function/method to a C# one
	 */
	template<typename Ret, typename T, typename... Args>
	void linkFunction(const std::string& name, Ret(*function)(T*, Args...))
	{
		// Create full signature from type pack
		std::string signature = resolveSignature<Args...>(name, {true, false, true});
		linkInternal(signature, function);
	}

	template<typename Ret,typename... Args>
	void linkFunctionStatic(const std::string& name, Ret(*function)(Args...))
	{
		// Create full signature from type pack
		std::string signature = resolveSignature<Args...>(name, { true, false, true });
		linkInternal(signature, function);
	}
private:
	template<typename... Args>
	std::string resolveSignature(const std::string& funcName, SignatureParams params = {false, false, false})
	{
		std::vector<std::string> types;
		std::string fullSignature = m_pObject->getNSpace() + "." + m_pObject->getName() +(params.Static ? "::" : ":") + funcName + "(" + (params.IncludeThis ? "intptr" : "");

		// Could be optimized
		resolveSignatureTypes<Args...>(types);
		if (types.size() > 0)
		{
			if (params.IncludeThis)
			{
				fullSignature += ",";
			}

			for(size_t i = 0; i < types.size() - 1; i++)
			{
				fullSignature += types[i] + ",";
			}
			fullSignature += types[types.size() - 1];
		}

		fullSignature += ")";
		return fullSignature;
	}

	template<typename... Args>
	void resolveSignatureTypes(std::vector<std::string>& types)
	{
		((types.push_back(TypeReflection::typeName<Args>())), ...);
	}

	void resolveClass();
	void resolveField(GenericField* pIField);
	void resolveMethod(GenericMethod* pIMethod);
	void resolveProperty(GenericProperty* pIProperty);
	void linkInternal(const std::string& name, void* lambdaCB);
private:
	bool m_valid = false;
	SharpBackend* m_pInterop;
	ScriptObject* m_pObject;
};

/**
 * Class used to define a script object on C++ side
 */
class ScriptObject : public IObject
{
public:
	ScriptObject(const std::string& assembly, const std::string& nspace, const std::string& name);
	virtual ~ScriptObject() {}

	void invokeDefaultCtor();

	// Inherited via IObject
	virtual bool init(IBackend* pBackend) override;
	virtual std::string getBackend() const override;
	virtual std::string getInstanceID() const override;

	const std::string& getAssembly() const;
	const std::string& getNSpace() const;
	const std::string& getName() const;
protected:
	virtual void resolve(ScriptBuilder script) = 0;

private:
	ScriptBuilder createBuilder(SharpBackend* pInterop);
private:
	std::string m_assembly;
	std::string m_nspace;
	std::string m_name;

	MonoClass* m_pClass;
	MonoObject* m_pInstance;
	MonoVTable* m_pVTable;

	friend ScriptBuilder;
};

/**
 * C++ C# interop engine
 */
class SharpBackend : public IBackend
{
	/**
	 * C# assembly data
	 */
	struct _Assembly
	{
		/// Path to the assembly
		std::string Path;

		/// Name of the assembly
		std::string Name;

		/// Assembly object pointer
		MonoAssembly* pAssembly;

		/// Image of the assembly
		MonoImage* pImage;
	};
public:
	SharpBackend();
	~SharpBackend();

	/**
	 * Loads assemblies added to the interop into a new domain
	 */
	void load(const std::string& assemblyPath);

	/**
	 * Unloads the interop, freeing all data
	 */
	void unload();

	/**
	 * Completely shutdowns the interop, all C# calls after this is
	 * undefined behavior
	 */
	void shutdown();

	// Inherited via IBackend
	virtual std::string getName() override;
	virtual bool init() override;
	virtual void newObject(IObject* pObject) override;
	virtual IObject* getObject(const std::string& instanceID) override;
private:
	Memory::reference<_Assembly> getAssembly(const std::string& name);
private:
	/// JIT Domain
	MonoDomain* m_pJITDomain;

	/// Current iteration domain
	MonoDomain* m_pItDomain;

	/// All added assembly paths
	std::vector<Memory::reference<_Assembly>> m_loadedAssemblies;
	std::vector<ScriptObject*> m_objects;

	friend ScriptBuilder;
};

/**
 * C# source
 */
class SharpSource : public ISource
{
public:
	SharpSource(std::string pathToAssembly);

	// Inherited via ISource
	virtual std::string getBackend() override;
	virtual bool load(IBackend* pBackend) override;

private:
	std::string m_assembly;
};
