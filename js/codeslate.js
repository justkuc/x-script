var fileTabCount = 0;
var openedTabs = 0;
var untitledCount = 0;

var fs = require('fs');
var fsExtra = require('fs-extra');
const hideFile = require('hidefile');
const { exec } = require("child_process");

var editorId;
var codeStatId;
var currentDir;
var currentFileAddress;
var editor;

function appendFileInExplorerContainer(fileAddress){
	//console.log("appending the files in explorer");
	explorerTabCount++;
	
	
	var tabId = "explorerTabId_"+explorerTabCount;
	var subId = "subId_"+explorerTabCount;
	var fileName = currentFileName;
	var fileAdd = fileAddress;

	
	$(".exploredFilesContainer").append("<li>"+returnExplorerDesign(tabId, fileAdd, fileName)+"</li> <div class='subFileContainer' id='"+subId+"'></div>");
	
};

function openCodeSlate(tabNumber) {
	
	editorId = "codeslate_" + tabNumber;
	codeStatId = "codestat_" + tabNumber;

	var editorStyles = "position: relative; top: 0; right: 0; bottom: 90; left: 0; font-size: 12pt;" +
	"font-weight: normal; white-space: nowrap; display:block; z-index: 999";

	var editorDesign = "<div class='codeslate' id='" + editorId + "' style='" + editorStyles + "'></div>";

	//var codeStatContent = "<div class='codeStat'><div class='currentLang'>C#</div></div>";

	//editorDesign += codeStatContent;

	$(".editorContainer").append(editorDesign);

	editor = ace.edit(editorId);
	editor.setTheme("ace/theme/monokai");
	editor.session.setMode("ace/mode/csharp");
	// codeSlate(tabNumber);
	// findEditor(editorId);
}

function getFileName(argName){
	var intermediate = argName.split('\\');
	return intermediate[intermediate.length - 1];
}

function selectFolder(){
	dialog.showOpenDialog({
		properties: ['openDirectory']},
		function(dir,e){
			if(dir!==undefined){
			console.log("opening dir: " + dialog.filePaths);
			console.log(e);
			$(".exploredFilesContainer").html("");

			var selectedDirectory = dir[0];	

			directoryPath = selectedDirectory;

			readDirectoryAt(selectedDirectory);
			
			$("#folderName").html(getFileName(selectedDirectory));

			fileObj.directoryPath = selectedDirectory;
			
			writeJson(fileObj);
			}
			else{
				console.log("No files selected");
			}	
	}).then(result => {
		currentDir = result.filePaths[0];
		syncFiles(result.filePaths[0]);
		$("#folderName").html(getFileName(result.filePaths[0]));
	});
}
	
function getFileExtension(argName){
	
	var fileName = getFileName(argName);
	var ext = fileName.split(".");
	return ext[ext.length-1];
}
	
function syncFiles(directory){
	if (typeof(directory) != 'string')
	{
		console.log(typeof(directory));
		$(".exploredFilesContainer").html("");
		readDirectoryAt(currentDir);
	}
	else {
		$(".exploredFilesContainer").html("");
		readDirectoryAt(directory);
	}
	var olddir = directory;
}

function returnExplorerDesign(tabId, fileAddress, fileName){

	var closingTabId = tabId;
	var des;
	var symbolId = "symbol_"+numberReturner(tabId);
	
	var functionToCall = '';
	
	var ext = getFileExtension(fileAddress);
	var symbol = '';
	
	if(ext===fileName){
		//when its a folder
		functionToCall = 'openFolder(id)';
		symbol = "<div class='symbol folderClosed' id='"+symbolId+"'></div>";
	}
	else
	{
		//when its  a file
		functionToCall = ModifyFileByExtension(ext);
		symbol = "<div class='symbol fileSymbol' id='"+symbolId+"'></div>";
	}
	
	des ="<div id='"+tabId+"' fileaddress='"+fileAddress+"' class='filenameSpan'";
	des += "oncontextmenu='contextMenuForExploredContainer(event,id);'";
	des += "onclick="+functionToCall+">"+symbol+"<span id='"+tabId+"_fileName'>"+fileName+"</span>";
	return des;
}


function readDirectoryAt(selectedDirectory){
	var i;	
	currentDir = selectedDirectory;	
	fs.readdir(selectedDirectory, function(err,dir){
		for(i=0; i<dir.length; i++){
			var folderFileAddress = selectedDirectory+"\\"+dir[i];
			currentFileName = getFileName(folderFileAddress);
			mode = getFileExtension(folderFileAddress).toLowerCase();
			appendFileInExplorerContainer(folderFileAddress);
		}
	});
}


function createNewFile(){
	mode = "csharp";
	$(".editorContainer div[id^='codeslate_']").css("display","block");
	$(".editorContainer div[id^='codestat']").css("display","block");
	savingAllowed = true;
	
	//adds new tab and sets the fileaddress to null in the explorer
	addNewExplorerTabInFilesContainer();
	//opens new coding screen
	openCodeSlate(fileTabCount);
	
	detectNewlyOpenedFile(fileTabCount);

	populateTitleText();
	//syncFiles(currentDir);
	
}

function addNewExplorerTabInFilesContainer(){

	fileTabCount++;
	openedTabs++;
	untitledCount++;
	
	var tabId = "tabId_"+fileTabCount;
	var fileAddress = "null";
	var fileName = "Untitled - "+untitledCount;
	
	
	$(".filesContainer").append("<li>"+returnListDesign(tabId,fileAddress,fileName,false)+"</li>");
	
	filePath = fileAddress;
	
}

function detectNewlyOpenedFile(getId){
	
	deselectAllFiles();
	
	var newTabId = "tabId_"+getId;
	var newTabAddress = $("#"+newTabId).attr("fileaddress");
	var selectedTabAddress = $("#"+selectedTabId).attr("fileaddress");
	
		$("#"+newTabId).parent().addClass("selectedFile");
		selectedTabId = newTabId;
	
}

function populateTitleText(){
	modifyTitleBar($("#"+selectedTabId).text());
}

function returnListDesign(tabId, fileAddress, fileName, fileSaved){
	var des =	"<span onclick='closeTab("+tabId+")' class='closeTabIcon'>X</span>" +
	   "<div id='"+tabId+"' fileaddress='"+fileAddress+"' class='filenameSpan'  onclick='toggleCodingScreensFromFilesContainer(id);'>"+fileName ; 
	   des += updateFileSave(fileSaved); 
	   
	   
	return des;
}

function updateFileSave(fileSaved){
	var statement;
	if(fileSaved)
	{	 
		statement = "<div class='writeIndicator' style='opacity:0;'></div></div>";	
	}
	else
	{
		statement = "<div class='writeIndicator' style='opacity:1;'></div></div>";	
	}
	return statement;
}


function toggleCodingScreensFromFilesContainer(explorerTabId){

	var tabNumber = numberReturner(explorerTabId);

	toggleCodeSlate(tabNumber);
	var exists = document.getElementById(tabNumber);
	// console.log(exists);
	deselectAllFiles();

	selectedTabId = explorerTabId;

	$(".filesContainer #"+explorerTabId).parent().addClass("selectedFile");

	populateTitleText();
}

function closeTab(tabtoClose){
	console.log(tabtoClose);
	//console.log(openedFiles);
	var closingTabId = tabtoClose.id;

	var currentTabNumber = numberReturner(selectedTabId);

	if(currentTabNumber!=1) currentTabNumber--;

	selectedTabId = "tabId_"+currentTabNumber;

	// console.log(closingTabId);
	closeTabLogic(closingTabId);
	// console.log("Id of selected tab: " + selectedTabId);
	toggleCodingScreensFromFilesContainer(selectedTabId);
} 


function numberReturner(id){
	return id.split("_")[1];
}

function closeCurrentTab(){
	//for current tab..
	// console.log(document.getElementsByClassName('selectedFile')[0].getElementsByTagName('div')[0]);
	
	var currentTabNumber = numberReturner(selectedTabId);
	// console.log(currentTabNumber);
	if(currentTabNumber!=1) currentTabNumber--;

	var closingTabId = selectedTabId;

	selectedTabId = "tabId_"+currentTabNumber;

	toggleCodingScreensFromFilesContainer(selectedTabId);

	closeTabLogic(closingTabId);
}

function closeTabLogic(closingTabId){
	// console.log(closingTabId);
	if(fileTabCount>0){      
		var relevantCodingSlateId = "codeslate_"+numberReturner(closingTabId);
		var relevantCodingStatId = "codestat_"+numberReturner(closingTabId);
		
		// $("#"+relevantCodingSlateId).remove();
		// $("#"+relevantCodingStatId).remove();
		
		// $("#"+closingTabId).parent().remove();
	}	

	var tab = "tabId_" + numberReturner(closingTabId);
	var slate = "codeslate_" + numberReturner(closingTabId);
	var stat = "codestat_" + numberReturner(closingTabId);

	
	var text = document.getElementById(tab).textContent;

	if (text.includes("Untitled"))
	{
		--untitledCount;
	}

	var address = document.getElementById(tab).getAttribute("fileaddress");
	document.getElementById(tab).parentElement.remove();
	document.getElementById(slate).remove();
	if (document.getElementById(stat))
	{
		document.getElementById(stat).remove();
	}


	var indexOfClosed = openedFiles.indexOf(address);
	if (indexOfClosed > -1){
		openedFiles.splice(indexOfClosed, 1);
	}

	// console.log(openedFiles);

	//--fileTabCount;
	--openedTabs;
	populateTitleText();
	checkIfThereAreNoMoreTabsLeft();
}

function checkIfThereAreNoMoreTabsLeft(){
	if(openedTabs==0){
		modifyTitleBar(" ");
	 }
}


function deselectAllFiles(element){
	
	if($(".filesContainer li").hasClass("selectedFile")){
		
	$(".filesContainer li").removeClass("selectedFile");
		
	}
}

function toggleCodeSlate(tabNumber){
	hideContentFromContainer();

	var codeslateId = "codeslate_"+tabNumber;
	var codestatId = "codestat_"+tabNumber;

	$("#"+codeslateId).css("display","block");
	$("#"+codestatId).css("display","block");
	 
	findEditor(codeslateId);
}

function hideContentFromContainer(){
	$(".editorContainer div[id^='codeslate_']").css("display","none");
	$(".editorContainer div[id^='codestat_']").css("display","none");
	$(".editorContainer div[id^='imageSlate_']").css("display","none");
	$(".editorContainer div[id^='videoSlate_']").css("display","none");
}

function findEditor(id,mode){
	//console.log("finding the editor");
	//console.log(id);
    editor = ace.edit(id);
	if(mode==="js"){
	   mode = "javascript";
	}
	if(mode==="ts"){
		mode = "typescript";
	}
	if(mode==="py"){
		mode = "python";
	}
	if(mode === "cs"){
		mode = "csharp";
	}

	$("#"+id).on("contextmenu",function(event){

		contextMenuForEditor(event,id);

	});	

	aceConfig(editor,id,mode);
	

	fs.readFile(jsonThemeFilePath,"utf-8",function(e,data){

	var parseJson = JSON.parse(data);

	editor.setTheme(parseJson.currentTheme);

	});
}

var menuOpen = false;
var menu = $("#contextMenu");
var contextLi;

function contextMenuForEditor(e,id){

	contextMenuProp(e);

	menu.append("<li onclick='copyFileAddress(id)'>Copy File Address</li>");
	menu.append("<li onclick='copyFileAddress(id)'>Copy File Address</li>");
	menu.append("<li onclick='copyFileAddress(id)'>Copy File Address</li>");
	menu.append("<li onclick='copyFileAddress(id)'>Copy File Address</li>");
	menu.append("<li class='separator'></li>");
	menu.append("<li onclick='copyFileAddress(id)'>Copy File Address</li>");
	menu.append("<li onclick='copyFileAddress(id)'>Copy File Address</li>");

}

function contextMenuProp(e){

	/******** menu properties ********/
	var ypos;
	if(e.clientY>555){
		ypos = e.clientY-menu.height();
	}else{
		ypos = e.clientY;
	}
	menu.css({
		top:ypos+"px",
		left:e.clientX+"px"
	});

	menu.html("");

	menuOpen = true;
	menu.show();

}

function contextMenuForExploredContainer(e,id,symbol){
	
	var currentfileaddress = $("#"+id).attr("fileaddress");

	contextMenuProp(e);

	contextLi = $("#"+id).parent();

	$(contextLi).addClass("contextSelected");

	var folder = $(".folderFiles");

	menu.append("<li id='deletefile_"+id+"' address='"+currentfileaddress+"' onclick='deleteCurrentFile(id)'>Delete File</li>");
	menu.append("<li id='renamefile_"+id+"' address='"+currentfileaddress+"' onclick='renameCurrentFile(id)'>Rename File</li>");

}

function aceConfig(editor,id,mode){
		
	editor.getSession().on('change', function() {
		
		var tabNum = id.split("_")[1];
		var tabId = "tabId_"+tabNum;
	
		
		var opacity = $("#"+tabId+" .writeIndicator");
		
		opacity.css("opacity",1);	
		
		characterLength = editor.session.getValue().length;
	});	
	
	
		 editor.session.setMode("../ace/mode/"+mode);
	
		
		 editor.setOptions({
			 enableBasicAutocompletion: true,
			 enableSnippets: true
		 });
		// pass options to ace.edit
	
	//ace.createEditSession("string or array", "ace/mode/javascript")
	
		
	editor.session.markUndoGroup(); 
	
		editor.find('function',{
		backwards: false,
		wrap: true,
		caseSensitive: true,
		wholeWord: false,
		regExp: true
	});
	
		
	editor.findNext();
	editor.findPrevious();
	
	editor.focus();
	editor.setShowPrintMargin(false);
	
	editor.getSession().setUseWrapMode(true)
}


function toggleCodingScreensFromFilesContainer(explorerTabId){

	var tabNumber = numberReturner(explorerTabId);

	toggleCodeSlate(tabNumber);

	deselectAllFiles();

	selectedTabId = explorerTabId;

	$(".filesContainer #"+explorerTabId).parent().addClass("selectedFile");

	populateTitleText();
}

//toggling coding screens from explorer container
function toggleCodingScreensFromExplorerContainer(explorerTabId){
		
	//check whether the current file is already in the filecontainer or not
	
		var tabNumber = numberReturner(explorerTabId);
		currentFileAddress = $("#"+explorerTabId).attr("fileaddress");

		openFile(currentFileAddress);
	
}


function ModifyFileByExtension(ext){

	switch(ext.toLowerCase()){
		case "html":case "css":case "txt": case "php": case "json":case "js":case "cs":
		case "php":case "py":case "xml":case "ts": case "cpp": case"java": case "dart":
		return 'toggleCodingScreensFromExplorerContainer(id)';
		
		case "png":case "PNG":case "JPG":case "jpg": case"jpeg": case "JPEG":case "psd":
		case "gif":
		case "webp":
		return 'togglePictureViewFromExplorerContainer(id)';

		case "mp4":case "flv":case "mkv":
		return 'toggleVideoViewFromExplorerContainer(id)';
	}
}

function openFolder(id){

	var listElement = $("#"+id);

	var subId = "subId_"+numberReturner(id);
	var address = listElement.attr("fileaddress");
	var symbolId = "symbol_"+numberReturner(id);

	$("#"+subId).html("");
	$("#"+subId).toggleClass("opened");

	if($("#"+subId).hasClass("opened")){

		$("#"+symbolId).removeClass("folderClosed");
		$("#"+symbolId).addClass("folderOpen");

	}else if(!$("#"+subId).hasClass("opened")){

		$("#"+symbolId).removeClass("folderOpen");
		$("#"+symbolId).addClass("folderClosed");

	}

	fs.readdir(address,function(err,dir){

			for(i=0; i<dir.length; i++){

        		var folderFileAddress = address+"\\"+dir[i];
			
				currentFileName = getFileName(folderFileAddress);

        		mode = getFileExtension(folderFileAddress).toLowerCase();

				appendFileInSubfileContainer(folderFileAddress,subId);
		
        	}

	});

}

function appendFileInSubfileContainer(fileAddress,subId){
	//console.log("appending the files in explorer");
	explorerTabCount++;
	
	var tabId = "tabId_"+explorerTabCount;
	var furtherSubId = "subId_"+explorerTabCount;
	var fileName = currentFileName;
	var fileAdd = fileAddress;

	$("#"+subId).append("<li>"+returnExplorerDesign(tabId, fileAdd, fileName)+"</li> <div class='subFileContainer' id='"+furtherSubId+"'></div>");
	
};

function universalFileOpener(theFileAddress){
	openFile(theFileAddress);
}


function thisIsTheFirstFile(currentFileAddress){
	var check = true;
	// console.log("=== IS this the first file check ==== ")
	// console.log("currentFileAddress = " + currentFileAddress);
	for(var i=0; i<=openedFiles.length; i++){

		if(currentFileAddress == openedFiles[i]){
			check = false;
			break;
		}
	}

	if (openedFiles.indexOf(currentFileAddress) > -1)
	{
		check = false;
	}
	return check;
}

function appendFileInFileContainer(fileAddress){
	//console.log("appending the files in explorer");
	fileTabCount++;
	openedTabs++;
	
	//jsonContent.fileTabCount = fileTabCount;
	
	//writeJson(jsonContent);
	
	var tabId = "tabId_"+fileTabCount;
	var fileName = currentFileName;
	var fileAdd = fileAddress;
	
	$(".filesContainer").append("<li>"+returnListDesign(tabId, fileAdd, fileName,true)+"</li>");
	
	
};

function readFile(filePath){
	// console.log("reading file");
	
	fs.readFile(filePath,'utf-8',function(e,data){
		//console.log("fs read called");
		editor.setValue(data);
	});
};

function push(theFileAddress){

	// console.log("push function has worked");

	openedFiles.push(theFileAddress);
}

function pop(fileTabId){
	var fileToPop = $("#"+fileTabId).attr("fileaddress");
	var index = openedFiles.indexOf(fileToPop);

	openedFiles.splice(index,1);
}

function modifyTitleBar(fileName){
	var fileNameComponent = document.getElementById("currentFileName");
	fileNameComponent.innerHTML = (openedTabs>0) ? (fileNameComponent.innerHTML = fileName ) : (fileNameComponent.innerHTML = fileName);
}

function openNewCodeSlate(tabNumber,filePath){
	codeSlate(tabNumber);
	var fileExt = getFileExtension(filePath);
	findEditor(editorId,fileExt);
}

function parseMode(mode){
	if(mode==="js"){
	   mode = "javascript";
	}
	if(mode==="ts"){
		mode = "typescript";
	}
	if(mode==="py"){
		mode = "python"
	}
	if(mode==="cs"){
		mode = "csharp"
	}

	return mode;
}

function hideMenu(menu){
	if(menuOpen){
		menu.hide();
		$(contextLi).removeClass("contextSelected");
	}

}

function codeSlate(tabNumber){

	editorId = "codeslate_"+tabNumber;
	codestatId = "codestat_"+tabNumber;
	
	var editorDesign = "<div class='codeslate' id='"+editorId+"' style='"+editorStyles+"'></div>";
	var codeStatContent = "<div class='currentLang'>"+parseMode(mode)+"</div>";
	editorDesign+="<div class='codeStat' id='"+codestatId+"'> "+codeStatContent+" </div>";
	$(".editorContainer").append(editorDesign);
	
	if(tabNumber>1)
	{  
		hideContentFromContainer();
	}

	$("#"+editorId).css("display","block");
	$("#"+codestatId).css("display","block");
}

// ----------------------- Saving file

function saveFile(){	
	if(savingAllowed){	
		if(filePath == 'null'){	
			showSaveDialogAndSaveFile();
		}
		else{
			saveFileWithoutDialog();
		}
	}
}


function showSaveDialogAndSaveFile(){		
	var content = editor.getValue();
	dialog.showSaveDialog((fileName) => {
		console.log("inside func started")
		if (fileName === undefined){
			console.log("File name was undefined, fail saved");
			return;
		}
	}).then(result => {
		fs.writeFile(result.filePath, content, (err) => {
			if (err) {
				alert("An error ocurred when creating the file " + err.message);
				console.log("error");
			}
			else{
				//alert("The file has been saved successfully");
				syncFiles(currentDir);
				closeTabLogic(selectedTabId);
				openFile(result.filePath);
				var opacity = $("#"+tabId+" .writeIndicator");
				opacity.css("opacity",0);
			}

		})
	});
}

function openFile(result) {
	if (thisIsTheFirstFile(result)) {
		filePath = result;
		push(result);
		savingAllowed = true;
		mode = getFileExtension(filePath).toLowerCase();
		currentFileName = getFileName(filePath);
		//modify the title bar in the file
		modifyTitleBar(currentFileName);
		//increases fileTabCount and puts the file in the explorer with the fileaddress provided from here
		appendFileInFileContainer(filePath);
		//responsible for the look and feel that tabs are being selected and new file also has fileselected class
		detectNewlyOpenedFile(fileTabCount);
		//reads the selected file and then sets it in the editor
		readFile(filePath);
		//opens the respective code slate for each tab
		openNewCodeSlate(fileTabCount, filePath);

		// var opacity = $("#"+tabId+" .writeIndicator");
		// opacity.css("opacity",0);
	}
}

function saveFileWithoutDialog(){
	if(savingAllowed){
		var content = editor.getValue();
		var tabIdNum = numberReturner(editor.container.id);	
		var tabId = "tabId_"+tabIdNum;
		var tabNr = $("#" + tabId)[0].getAttribute("fileaddress");; //+ " .filenamespan")[tabIdNum];//.getAttribute("fileaddress");
		var filePath = tabNr;

		fs.writeFile(filePath, content, (err) => {

			if (err) {
				alert("An error ocurred updating the file" + err.message);
				console.log(err);
				fileSaved = false;
				updateFileSave(false);
				return;	
			}
			// console.log(filePath);
			// console.log(content);
			
			var opacity = $("#"+tabId+" .writeIndicator");

			opacity.css("opacity",0);
			// fileSaved = true;
			// updateFileSave(fileSaved);
			
			// var fileExt = getFileExtension(filePath);

			// mode = parseMode(fileExt);
		});
	}	
}

// ----------------------- Context menu stuff

function deleteCurrentFile(fileId){
	
	var addressOfFileToBeDeleted = $("#"+fileId).attr("address");
	var split = fileId.split("_");
	console.log(fileId);
	var tabId = split[1]+"_"+split[2];
	var id = $("#"+tabId);
	console.log(tabId);
	console.log(fileId);
	var tabNr = $("div.filenameSpan");
	//var tabNr = tabNr[0].getAttribute("fileaddress");

	//[fileaddress='"+addressOfFileToBeDeleted+"']
	//var tabNr = document.getElementsByClassName("filenameSpan");
	fs.unlink(addressOfFileToBeDeleted, (err) => {
  		if (err) throw err;
  		
  		$("#"+tabId).fadeOut("fast");

	});

	for (var i = 0; i<tabNr.length; i++)
	{
		//console.log(tabNr[i]);
		if (tabNr[i].getAttribute("fileaddress") == addressOfFileToBeDeleted)
		{
			var containerTabId = tabNr[i].getAttribute("id");
			break;
		}
	}
	console.log(containerTabId);
	closeTabLogic(containerTabId);
	//console.log(tabNr);
	console.log(currentDir);
	syncFiles(currentDir)
}

function renameCurrentFile(fileId) {
	var addressOfFileToBeRenamed = $("#"+fileId).attr("address");
	var fileName = getFileName(addressOfFileToBeRenamed);
	var split = fileId.split("_");
	var tabId = split[1]+"_"+split[2];

	$("#renameFileContainer").css("display","block");
	$("#renameInput").focus();

	$(document).on("keydown",function(e){
		if (e.keyCode===13) {
			//renamefile
	
		var newName = $("#renameInput").val();
		
		var newAddress = addressOfFileToBeRenamed.replace(fileName,newName);

		$("#"+tabId).attr("fileaddress",newAddress);
		$("#"+tabId+"_fileName").html(newName);

			fs.rename(addressOfFileToBeRenamed, newAddress, (err) => {
  				if (err) throw err;
			});
			$("#renameFileContainer").css("display","none");
		}
		else if (e.keyCode === 27){
			$("#renameFileContainer").css("display","none");
		}
	});
}

function createNewProject() {
	$("#projectNameContainer").css("display", "block");
	$("#projectNameInput").focus();

	const dirs = [
		'.build',
		'.build\\GeneratedProjects',
		'.build\\Sources',
		'.build\\Sources\\Sharp',
		'.build\\Sources\\Python',
		'.build\\Sources\\cpp',
		'out',
		'src',
		'src\\Python',
		'src\\Sharp'
	]

	const dependencies = [
		'mono',
		'mono-2.0-sgen.dll',
		'python38.dll'
	]

	var projectDir = path.join(__dirname, "\\Projects\\");
	console.log(projectDir);
	$(document).on("keydown", function(e){
		if (e.keyCode === 13) {
			var projectName = $("#projectNameInput").val();
			var fullProjectDir = projectDir + projectName + "\\";
			console.log(fullProjectDir);


			if (!fs.existsSync(fullProjectDir))
			{
				dirs.forEach(directory => {
					var concatenated = fullProjectDir + directory + "\\";
					fs.mkdirSync(concatenated, { recursive: true });
					//console.log(fullProjectDir + directory);
				})
			}
			hideFile.hideSync(fullProjectDir + '.build');
			//hideFile.hideSync(fullProjectDir + '.tools');

			syncFiles(fullProjectDir);
			var name = fullProjectDir.substring(0, fullProjectDir.length -1);
			$("#folderName").html(getFileName(name));

			document.getElementById("projectNameInput").value = "";
			$("#projectNameContainer").css("display","none");

			var toolsDir = path.join(__dirname, "\\Tools\\");

			var cppDir = path.join(__dirname, "\\Kernel\\cpp");
			var bakedProjectsDir = path.join(toolsDir, "\\BakedProjects");
			var currProjGeneratedProjects = path.join(currentDir, "\\.build\\GeneratedProjects\\");
			var hiddenSources = path.join(currentDir, "\\.build\\Sources\\cpp");
			var outDir = path.join(currentDir, "\\out");
			fsExtra.copy(bakedProjectsDir, currProjGeneratedProjects, (err) => {
				if (err)
				{
					console.log(err)
				}
				else
				{
					// console.log('successfully copied from bakedProjects');
					// var SharpLinker = path.join(toolsDir, "SharpLinker.py")
					// var callSharpLinker = `python ` + SharpLinker + ` ` + currentDir + `\\.build\\Sources\\Sharp\\ ` + currentDir + `\\.build\\GeneratedProjects\\sharp_glue.csproj`;
					// console.log("Successfully called SharpLinker ");
					fsExtra.copy(cppDir, hiddenSources, (err) => {
						if (err)
						{
							console.log(err)
						}
						else
						{
							console.log("Successfully copied cpp");
						}
					});
					dependencies.forEach(dependency => {
						fsExtra.copy(path.join(toolsDir, dependency), path.join(outDir, dependency), (err) => {
							if (err)
							{
								console.log(err)
							}
							else
							{
								console.log("");
							}
						})
					})
				}; 
			});
		}
		else if (e.keyCode === 27){
			document.getElementById("projectNameInput").value = "";
			$("#projectNameContainer").css("display","none");
		}
	})
}

function buildProject() {
	var toolsDir = path.join(__dirname, "\\Tools\\");
	var	interpreterExe = path.join(toolsDir, "Interpreter\\XlangFramework.exe");
	var interpreterArgs = path.join(currentDir, "\\src\\") + " " + path.join(currentDir, "\\.build\\Sources\\");
	var inclGen = path.join(toolsDir, "InclGenerator.py");
	var inclGenArgs = path.join(currentDir, "\\.build\\Sources\\");
	var pythonSources = path.join(currentDir, '\\src\\Python\\');
	var outDir = path.join(currentDir, '\\out\\');
	console.log(interpreterExe);
	console.log(interpreterArgs);
	exec(interpreterExe + " " + interpreterArgs, (error, stdout, stderr) => {
				if (error)
				{
					console.log(error);
				}
				else
				{
					console.log(stdout);
					exec(`python ` + inclGen + " " + inclGenArgs, (error, stdout, stderr) => {
						if (error)
						{
							console.log(error);
						}
						else
						{
							console.log(stdout);
							var SharpLinker = path.join(toolsDir, "SharpLinker.py")
							var callSharpLinker = `python ` + SharpLinker + ` ` + currentDir + `\\.build\\Sources\\Sharp\\ ` + currentDir + `\\.build\\GeneratedProjects\\sharp_glue.csproj`;
							exec(callSharpLinker, (error, stdout, stderr) => {
								if (error)
								{
									console.log(error);
								}
								else if (stderr)
								{	
									console.log(stderr);
								}
								else
								{
									console.log(stdout);
										exec(`D: && call "D:\\Microsoft Visual Studio19\\VC\\Auxiliary\\Build\\vcvars64.bat" && cd "` + currentDir + `\\.build\\GeneratedProjects" && MSBuild.exe cpp_kernel.vcxproj -property:Configuration=Debug && MSBuild.exe sharp_glue.csproj`,
										   (error, stdout, stderr) => {
												if (error) {
													console.log(`error: ${error.message}`);
													return;
												}
												if (stderr) {
													console.log(`stderr: ${stderr}`);
													return;
												}
												if (stdout) {
													//console.log(`stdout: ${stdout}`);
													fs.copyFile(currentDir + '\\.build\\out\\bin\\cpp_kernel.exe', currentDir + '\\out\\output.exe', (err) => {
														if (err) throw err;
														console.log('cpp(exe) was copied to destination');
													})
													fs.copyFile(currentDir + '\\.build\\out\\bin\\sharp_glue.dll', currentDir + '\\out\\sharp_glue.dll', (err) => {
														if (err) throw err;
														console.log('sharp was copied to destination');
													})
													fsExtra.copy(pythonSources, outDir, (err) => {
														if (err) throw err;
														console.log('python files were copied to destination');
													})
												}
										});
								}
							});
						}
					})
				}
	});
	console.log(currentDir);
}



// ----------------------- JQuery
$( function() {
	 
    $( "#resizableExplorer" ).resizable({
		
	  maxHeight: 200,
      minWidth:2,
      minHeight: 200,
      handles: 'e'
		
	});
	 
	$(".explorerHeader").html("Current Files");
	 
	$("#changeFolder").on("click",selectFolder);

	$("#syncFiles").on("click",function() {
		syncFiles(currentDir)
	});

	$("body").on("click",function(){
	hideMenu(menu);
	});

	if(widgetsShown){
	$("#widgetCheckbox").attr("checked","true");
	}
});

$(".exploredFilesContainer").scroll(function()
{
	if(this.scrollTop > 8)
	{
		$(this).css("box-shadow","inset 0px 0px 5px rgba(0,0,0,0.5)");
	}
	else
	{
		$(this).css("box-shadow","inset 0px 0px 5px transparent");
	}

});
