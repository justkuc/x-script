#include <iostream>
#include <string>

// XLang runtime
#include "Languages/Runtime.h"

// Back-ends
#include "Languages/Python/PythonBackend.h"
#include "Languages/Sharp/SharpBackend.h"

// Generated content
#include "Dynamic/Incl.h"

int main()
{
	// Configure the runtime
	runtime.loadBackend<SharpBackend>();
	runtime.loadBackend<PythonBackend>();

	// Add sources
	xlang_add_sources();

	// Start the runtime
	runtime.start();

	// Invoke main
	xlang_invoke_main();

	return 0;
}