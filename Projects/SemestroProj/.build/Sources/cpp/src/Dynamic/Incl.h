#pragma once

// Create runtime
XLangRuntime runtime;

// Includes
#include <Dynamic/python_file.h>
#include <Dynamic/PythonInvokeTest.h>

// Sources
void xlang_add_sources()
{
	// Add code sources
	runtime.addSource<PythonSource>("python_file");
	runtime.addSource<SharpSource>("sharp_glue.dll");
}

// Entry point
void xlang_invoke_main()
{
	// Link objects
	runtime.newObject<python_file>();
	PythonInvokeTest* pMain = runtime.newObject<PythonInvokeTest>();

	// Invoke main
	pMain->Main->invoke();
}