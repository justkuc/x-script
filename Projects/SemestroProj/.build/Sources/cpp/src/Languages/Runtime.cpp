#include "Runtime.h"

#include <iostream>

XLangRuntime::~XLangRuntime()
{
	// Free memory
	for (auto& pair : m_backends)
	{
		delete pair.second;
	}

	for (auto& source : m_sources)
	{
		delete source;
	}
}

void XLangRuntime::start()
{
	// Init back-ends
	for (auto& pair : m_backends)
	{
		if (!pair.second->init())
		{
			std::cout << "[Error] Backend: " << pair.first << " failed to initialize!" << std::endl;
		}
	}

	// TODO: Smart loading
	// Load sources
	for (auto& source : m_sources)
	{
		if (!source->load(m_backends[source->getBackend()]))
		{
			std::cout << "[Error] Backend: " << source->getBackend() << " was not found when loading a source!" << std::endl;
		}
	}
}

IObject* XLangRuntime::getObject(const std::string& instanceID)
{
	// Search for the instance id in loaded backends
	for (auto& pair : m_backends)
	{
		// Get object
		IObject* obj = pair.second->getObject(instanceID);
		if (obj != nullptr)
		{
			return obj;
		}
	}

	return nullptr;
}
