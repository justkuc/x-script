#include "PythonBackend.h"

PythonBackend::~PythonBackend()
{
	for (auto& object : m_objects)
	{
		delete object;
	}

	for (auto& moduleHandle : m_modules)
	{
		delete moduleHandle;
	}

	// Shutdown interpreter
	py::finalize_interpreter();
}

void PythonBackend::importModule(std::string path)
{
	Internal::ModuleHandle* pHandle = new Internal::ModuleHandle();
	pHandle->Module = py::module::import(path.c_str());
	m_modules.push_back(pHandle);
}

std::string PythonBackend::getName()
{
	return "Python";
}

bool PythonBackend::init()
{
	// Initialize python interpreter
	py::initialize_interpreter();

	return true;
}

void PythonBackend::newObject(IObject* pObject)
{
	// Initialize object and add it to the object vector
	PythonObject* pPython = static_cast<PythonObject*>(pObject);
	pPython->init(this);
	m_objects.push_back(pPython);
}

IObject* PythonBackend::getObject(const std::string& instanceID)
{
	for (IObject* obj : m_objects)
	{
		if (obj->getInstanceID() == instanceID)
		{
			return obj;
		}
	}

	return nullptr;
}

PythonSource::PythonSource(std::string filePath)
	: m_filePath(filePath)
{
}

std::string PythonSource::getBackend()
{
	return "Python";
}

bool PythonSource::load(IBackend* pBackend)
{
	PythonBackend* pPython = static_cast<PythonBackend*>(pBackend);
	pPython->importModule(this->m_filePath);

	return true;
}

PythonObject::PythonObject(std::string module)
	: m_moduleName(module)
{
}

std::string PythonObject::getBackend() const
{
	return "Python";
}

bool PythonObject::init(IBackend* pBackend)
{
	PythonBackend* pPython = static_cast<PythonBackend*>(pBackend);

	// TODO: Return if it loaded successfully
	resolve(PythonBuilder(pPython, this));

	return true;
}

const std::string& PythonObject::getModuleName() const
{
	return m_moduleName;
}

std::string PythonObject::getInstanceID() const
{
	return "Undefined";
}

PythonBuilder::PythonBuilder(PythonBackend* pBackend, PythonObject* pObject)
	: m_pBackend(pBackend), m_pObject(pObject)
{
	// Resolve module
	for (Internal::ModuleHandle* module : m_pBackend->m_modules)
	{
		if (pObject->m_moduleName == module->Module.attr("__name__").cast<std::string>())
		{
			pObject->m_module = module;
			m_valid = true;
			break;
		}
	}
}
