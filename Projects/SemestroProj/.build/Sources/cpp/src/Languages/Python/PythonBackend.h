#pragma once

#include "Languages/BackendInterface.h"
#include "Utility/Memory.h"

#include <pybind11/pybind11.h>
#include <pybind11/embed.h>
namespace py = pybind11;

class PythonBackend;
class PythonSource;
class PythonObject;
class PythonBuilder;

namespace Internal
{
	/**
	 * Used to provide reference to a single module with a pointer
	 */
	struct ModuleHandle
	{
		py::module Module;
	};
}

template<typename Ret, typename... Args>
class PythonMethod
{
public:
	PythonMethod() {}
	PythonMethod(const std::string& signature)
		: m_signature(signature)
	{}

	virtual ~PythonMethod()
	{}

	const std::string& getSignature() const
	{
		return m_signature;
	}

	Ret invoke(Args&... params)
	{
		return m_module->Module.attr(m_signature.c_str())(params...).cast<Ret>();
	}
private:
	std::string m_signature;
	Internal::ModuleHandle* m_module;
	PythonObject* m_pObject = nullptr;

	friend PythonBuilder;
};


template<typename T>
class PythonField
{
public:
	PythonField() {}
	PythonField(const std::string& name)
		: m_name(name)
	{}

	virtual ~PythonField()
	{}

	const std::string& getName() const
	{
		return m_name;
	}

	Memory::managed_ptr<T> get()
	{
		return m_module->Module.attr(m_name.c_str()).cast<T>();
	}

	void set(T* pValue)
	{
		m_module->Module.attr(m_name.c_str()) = *pValue;
	}

	PythonField<T>& operator=(T* pNewValue)
	{
		set(pNewValue);
		return *this;
	}

	PythonField<T>& operator=(T newValue)
	{
		set(&newValue);
		return *this;
	}
private:
	std::string m_name;
	Internal::ModuleHandle* m_module;
	PythonObject* m_pObject = nullptr;

	friend PythonBuilder;
};


/**
 * C++ Python interop engine
 */
class PythonBackend : public IBackend
{
public:
	~PythonBackend();

	/**
	 * [Temporary]
	 * Import module into the back-end
	 */
	void importModule(std::string path);

	// Inherited via IBackend
	virtual std::string getName() override;
	virtual bool init() override;
	virtual void newObject(IObject* pObject) override;
	virtual IObject* getObject(const std::string& instanceID) override;
private:
	/**
	 * Modules loaded into the back-end
	 */
	std::vector<Internal::ModuleHandle*> m_modules;
	std::vector<PythonObject*> m_objects;
	friend PythonBuilder;
};
 
/**
 * Python code source
 */
class PythonSource : public ISource
{
public:
	PythonSource(std::string filePath);

	// Inherited via ISource
	virtual std::string getBackend() override;
	virtual bool load(IBackend* pBackend) override;
private:
	std::string m_filePath;
};


/**
 * Python script object
 */
class PythonObject : public IObject
{
public:
	PythonObject(std::string module);

	// Inherited via IObject
	virtual std::string getBackend() const override;
	virtual bool init(IBackend* pBackend) override;
	virtual std::string getInstanceID() const override;

	const std::string& getModuleName() const;
protected:
	virtual void resolve(PythonBuilder script) = 0;
private:
	std::string m_moduleName;
	Internal::ModuleHandle* m_module;
	friend PythonBuilder;
};


/**
 * Builder used to create python objects
 */
class PythonBuilder
{
public:
	PythonBuilder(PythonBackend* pBackend, PythonObject* pObject);

	bool isValid() const
	{
		return m_valid;
	}

	/**
	 * Returns python method
	 */
	template<typename Ret, typename... Args>
	Memory::reference<PythonMethod<Ret, Args...>> resolveMethod(const std::string& funcName)
	{
		if (!m_valid)
		{
			return nullptr;
		}

		// Create method and return it
		PythonMethod<Ret, Args...>* pMethod = new PythonMethod<Ret, Args...>(funcName);
		pMethod->m_module = m_pObject->m_module;
		pMethod->m_pObject = m_pObject;

		return Memory::reference<PythonMethod<Ret, Args...>>(pMethod);
	}

	/**
	 * Returns python field
	 */
	template<typename T>
	Memory::reference<PythonField<T>> resolveField(const std::string& name)
	{
		if (!m_valid)
		{
			return nullptr;
		}

		// Create method and return it
		PythonField<T>* pField = new PythonField<T>(name);
		pField->m_module = m_pObject->m_module;
		pField->m_pObject = m_pObject;

		return Memory::reference<PythonField<T>>(pField);
	}
private:
	bool m_valid = false;
	PythonBackend* m_pBackend;
	PythonObject* m_pObject;
};
