#pragma once

#include <string>

class XLangRuntime;
class IBackend;
class IObject;
class ISource;

/**
 * Interface for language back-end main classes
 */
class IBackend
{
public:
    virtual ~IBackend() {}

    /**
     * Get the name of the back-end
     */
    virtual std::string getName() = 0;

    /**
     * Initialize the back-end and return true if it was successfully initialized
     * false otherwise
     */
    virtual bool init() = 0;

    /**
     * Add a script object to the back-end
     */
    virtual void newObject(IObject* pObject) = 0;

    /**
     * Returns the object with the specified instance id
     */
    virtual IObject* getObject(const std::string& instanceID) = 0;
};

/**
 * Interface for a code source used by a back-end
 */
class ISource
{
public:
    virtual ~ISource() {}

    /**
     * Get the back-end for this source
     */
    virtual std::string getBackend() = 0;

    /**
     * Load the source into memory and return true if it was successful
     * false otherwise, the runtime manages when to load the source and
     * passes the back-end whose name is specified by the getName function
     */
    virtual bool load(IBackend* pBackend) = 0;
};

/**
 * Interface for a common language script object
 */
class IObject
{
public:
    virtual ~IObject() {}

    /**
     * Get the back-end for this object
     */
    virtual std::string getBackend() const = 0;

    /**
     * Create the object using its specified back-end
     */
    virtual bool init(IBackend* pBackend) = 0;

    /**
     * Return instance id for the object, used for the runtime to route
     * invokes
     */
    virtual std::string getInstanceID() const = 0;
};
