#pragma once

// For the reference count
#include <atomic>

namespace Memory
{
	/**
	 * Simple pointer wrapper that doesn't allow the deletion of the underlying pointer.
	 */
	template <typename T>
	class relay_ptr
	{
	public:
		/**
		 * Constructor with pointer
		 *
		 * @param object The pointer to be stored
		 */
		relay_ptr(T* pObject)
			: m_pObject(pObject)
		{ }

		/**
		 * Empty constructor
		 */
		relay_ptr()
			: m_pObject(nullptr)
		{ }

		/**
		 * Copy constructor
		 *
		 * @param other Another relay object
		 */
		relay_ptr(const relay_ptr<T>& relay)
		{
			m_pObject = relay.m_pObject;
		}

#ifdef _MEMORY_
		/**
		 * Create relay_ptr from unique_ptr
		 *
		 * @param aptr std::unique_ptr with T value
		 */
		relay_ptr(std::unique_ptr<T>& aptr)
			: m_pObject(aptr.get())
		{ }

		/**
		 * Create relay_ptr from shared_ptr
		 *
		 * @param aptr std::shared_ptr with T value
		 */
		relay_ptr(std::shared_ptr<T>& aptr)
			: m_pObject(aptr.get())
		{ }
#endif

		/**
		 * Destructor
		 */
		~relay_ptr()
		{
			if (m_pObject)
			{
				delete m_pObject;
			}
		}

		/**
		 * Set relay_ptr value to different ptr, the value of the original ptr is not changed
		 *
		 * @param other New value for this relay object
		 * @return This, but with new values
		 */
		relay_ptr<T>& operator=(const relay_ptr<T>& rhs)
		{
			m_pObject = rhs.m_pObject;
			return *this;
		}

		/**
		 * Set relay_ptr value to different ptr, the value of the original ptr is not changed
		 *
		 * @param other New value for this relay object
		 * @return This, but with new values
		 */
		relay_ptr<T>& operator=(T* pNewObject)
		{
			m_pObject = pNewObject;
			return *this;
		}

		/**
		 * Compares two relay pointers of type T by checking
		 * if the underlying pointers point to the same memory
		 *
		 * @param other The other relay pointer to compare to
		 * @return True if both relays point to the same memory
		 */
		bool operator==(const relay_ptr<T>& other) const
		{
			return m_pObject == other.m_pObject;
		}

		/**
		 * Checks if the underlying pointer is valid
		 *
		 * @return True if the pointer is not pointing to nullptr
		 */
		bool valid() const
		{
			return m_pObject != nullptr;
		}

		/**
		 * Member access operator
		 *
		 * @return Underlying object const pointer
		 */
		T* operator->() const
		{
			return m_pObject;
		}

		/**
		 * Creates a relay_ptr of specified type from current relay_ptr
		 * used in polymorphic types
		 *
		 * @tparam AsType The type of the new reference
		 * @return Relay_ptr with the specified AsType type
		 */
		template <typename AsType>
		relay_ptr<AsType> as()
		{
			return relay_ptr<AsType>(m_pObject);
		}
	private:
		T* m_pObject;
	};

	/**
	 * A pointer wrapper similar to std::shared_ptr, in that it counts reference count,
	 * but it also provides functionality for polymorphism with as() function
	 */
	template <typename T>
	class reference
	{
	private:
		// ref count manipulation - increase by 1
		void increment()
		{
			(*m_refCount)++;
		}

		// ref count manipulation - decrease by 1
		int decrement()
		{
			return --(*m_refCount);
		}
	private:
		T* m_pObject{ nullptr };
		std::atomic_int* m_refCount{ nullptr };

		template<class AsType>
		friend class reference;
	private:
		// Private constructor;
		reference(T* object, std::atomic_int* refcnt)
			: m_pObject{ object }
			, m_refCount{ refcnt }
		{
			increment();
		}
	public:
		/**
		 * Empty constructor
		 */
		reference()
			: m_pObject{ nullptr }
			, m_refCount{ nullptr }
		{
		}

		/**
		 * Constructor with pointer
		 *
		 * @param object The pointer to be stored
		 */
		reference(T* object)
			: m_pObject{ object }
			, m_refCount{ new std::atomic_int(0) }
		{
			increment();
		}

		/**
		 * Destructor
		 */
		virtual ~reference()
		{
			if (m_refCount)
			{
				int decrementedCount = decrement();
				if (decrementedCount <= 0)
				{
					delete m_refCount;
					delete m_pObject;
					m_refCount = nullptr;
					m_pObject = nullptr;
				}
			}
		}

		/**
		 * Copy constructor
		 *
		 * @param other Another reference object
		 */
		reference(const reference<T>& other)
			: m_pObject{ other.m_pObject }
			, m_refCount{ other.m_refCount }
		{
			if (m_refCount)
			{
				increment();
			}
		}

		/**
		 * Assignment operator used to reassign the value of the reference object
		 *
		 * @param other New value for this reference object
		 * @return This, but with new values
		 */
		reference<T>& operator=(const reference<T>& other)
		{
			if (this != &other)
			{
				if (m_refCount)
				{
					if (decrement() == 0)
					{
						delete m_refCount;
						delete m_pObject;
					}
				}
				m_pObject = other.m_pObject;
				m_refCount = other.m_refCount;
				increment();
			}
			return *this;
		}

		/**
		 * Compares two references of type T by checking
		 * if the underlying pointers point to the same memory
		 *
		 * @param other The other reference to compare to
		 * @return True if both reference point to the same memory
		 */
		bool operator==(const reference<T>& other) const
		{
			return m_pObject == other.m_pObject;
		}

		/**
		 * Dereference operator used to get the object instance
		 *
		 * @return Underlying object c++ reference
		 */
		T& operator*()
		{
			return *m_pObject;
		}

		/**
		 * Member access operator
		 *
		 * @return Underlying object const pointer
		 */
		T* operator->() const
		{
			return m_pObject;
		}

		/**
		 * Creates a reference of specified type from current reference
		 * used in polymorphic types
		 *
		 * @tparam AsType The type of the new reference
		 * @return Reference with the specified AsType type
		 */
		template <typename AsType>
		reference<AsType> as()
		{
			return reference<AsType>(dynamic_cast<AsType*>(m_pObject), m_refCount);
		}

		/**
		 * Checks if the underlying pointer is valid
		 *
		 * @return True if the pointer is not pointing to nullptr
		 */
		bool valid() const
		{
			return m_pObject != nullptr;
		}

		/**
		 * Returns the underlying pointer as a relay_ptr which guarantees that the pointer won't be deleted
		 * but also there won't be any reference counting which means that if the reference is deleted the
		 * relay_ptr will be invalid
		 *
		 * @return Relay pointer created from the underlying pointer
		 */
		relay_ptr<T> asRelay() const
		{
			return relay_ptr<T>(m_pObject);
		}

		/**
		 * Current reference count of the object
		 *
		 * @return Number of references
		 */
		int refCount() const
		{
			return *m_refCount;
		}
	};

	/**
	 * Simple pointer wrapper that will destroy the underlying pointer when destroyed unless its freed before
	 */
	template <typename T>
	class managed_ptr
	{
	public:
		/**
		 * Constructor with value
		 *
		 * @param object The value to be stored
		 */
		managed_ptr(T object)
			: m_pObject(new T())
		{ 
			*m_pObject = object;
		}

		/**
		 * Constructor with pointer
		 *
		 * @param object The pointer to be stored
		 */
		managed_ptr(T* pObject)
			: m_pObject(pObject)
		{ }

		/**
		 * Empty constructor
		 */
		managed_ptr()
			: m_pObject(nullptr)
		{ }

		/**
		 * Copy constructor
		 *
		 * @param other Another managed object
		 */
		managed_ptr(const managed_ptr<T>& ptr)
		{
			m_pObject = ptr.m_pObject;
		}

		/**
		 * Destructor
		 */
		~managed_ptr()
		{
			if (m_pObject)
			{
				delete m_pObject;
			}
		}

		managed_ptr<T>& operator=(const managed_ptr<T>& rhs) = delete;
		managed_ptr<T>& operator=(T * pNewObject) = delete;

		/**
		 * Compares two managed pointers of type T by checking
		 * if the underlying pointers point to the same memory
		 *
		 * @param other The other managed pointer to compare to
		 * @return True if both managed pointers point to the same memory
		 */
		bool operator==(const managed_ptr<T>& other) const
		{
			return m_pObject == other.m_pObject;
		}

		/**
		 * Checks if the underlying pointer is valid
		 *
		 * @return True if the pointer is not pointing to nullptr
		 */
		bool valid() const
		{
			return m_pObject != nullptr;
		}

		/**
		 * Member access operator
		 *
		 * @return Underlying object const pointer
		 */
		T* operator->() const
		{
			return m_pObject;
		}

		/**
		 * Dereference operator
		 *
		 * @return Underlying object const pointer
		 */
		T* operator*() const
		{
			return m_pObject;
		}

		/**
		 * Get reference to the underlying pointer
		 */
		T& ref() const
		{
			return *m_pObject;
		}

		/**
		 * Returns the underlying pointer, but invalidates this pointer meaning the lifetime
		 * is now managed by whoever freed this pointer
		 */
		T* free()
		{
			T* ret = m_pObject;
			m_pObject = nullptr;
			return ret;
		}
	private:
		T* m_pObject;
	};
}