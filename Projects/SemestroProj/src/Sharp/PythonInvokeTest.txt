%python_file%

using System;


namespace SharpGlue
{
	class PythonInvokeTest
	{
	    static void Main()
	    {
	        string tekstas = python_file.get_output_text("Pristatymas", 90);
	        python_file.print_custom_text(tekstas);
	    }
	}
}