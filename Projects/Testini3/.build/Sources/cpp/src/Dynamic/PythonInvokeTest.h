class PythonInvokeTest : public ScriptObject
{
public:
   PythonInvokeTest() : ScriptObject("sharp_glue","SharpGlue","PythonInvokeTest"){}


virtual std::string getInstanceID() const override{
	return "PythonInvokeTest";
}protected:
static MonoString*  get_output_text(MonoString*  prefix,int* number)
{
std::string cpp_prefix = TypeConversion<std::string>((MonoObject*)prefix).Value;
IObject* pObject = runtime.getObject("python_file");
python_file* pCasted = static_cast<python_file*>(pObject);
std::string result = pCasted->get_output_text->invoke(cpp_prefix,*number);
return TypeConversion<std::string>(result).pMonoValue;}

static  void print_custom_text(MonoString*  text)
{
std::string cpp_text = TypeConversion<std::string>((MonoObject*)text).Value;
IObject* pObject = runtime.getObject("python_file");
python_file* pCasted = static_cast<python_file*>(pObject);
pCasted->print_custom_text->invoke(cpp_text);
}

   virtual void resolve(ScriptBuilder script) override
{
invokeDefaultCtor();

auto par = script.resolveField<PythonInvokeTest*>("Instance");
script.linkFunctionStatic("__internal_get_output_text", PythonInvokeTest::get_output_text);
script.linkFunctionStatic("__internal_print_custom_text", PythonInvokeTest::print_custom_text);
par->setValue(this);



Main = script.resolveMethod<void>("Main");




}public:

Memory::reference<Field<std::string>> tekstas;


Memory::reference<Method<void>> Main;




};