class python_file : public PythonObject
{
public:
   python_file() : PythonObject("python_file"){}


virtual std::string getInstanceID() const override{
	return "python_file";
}protected:
   virtual void resolve(PythonBuilder script) override
{




get_output_text = script.resolveMethod<std::string,std::string,int>("get_output_text");
print_custom_text = script.resolveMethod<void,std::string>("print_custom_text");




}public:



Memory::reference<PythonMethod<std::string,std::string,int>> get_output_text;
Memory::reference<PythonMethod<void,std::string>> print_custom_text;




};