#include "SharpBackend.h"

#include <fstream>
#include <iostream>

ScriptObject::ScriptObject(const std::string& assembly, const std::string& nspace, const std::string& name)
	: m_assembly(assembly), m_nspace(nspace), m_name(name), m_pClass(nullptr), m_pInstance(nullptr), m_pVTable(nullptr)
{
}

void ScriptObject::invokeDefaultCtor()
{
	mono_runtime_object_init(m_pInstance);
}

bool ScriptObject::init(IBackend* pBackend)
{
	SharpBackend* pSharp = static_cast<SharpBackend*>(pBackend);

	// TODO: Return if it loaded successfully
	resolve(ScriptBuilder(pSharp, this));

	return true;
}

const std::string& ScriptObject::getAssembly() const
{
	return m_assembly;
}

const std::string& ScriptObject::getNSpace() const
{
	return m_nspace;
}

const std::string& ScriptObject::getName() const
{
	return m_name;
}

ScriptBuilder ScriptObject::createBuilder(SharpBackend* pInterop)
{
	return ScriptBuilder(pInterop, this);
}

std::string ScriptObject::getInstanceID() const
{
	return "Undefined";
}

std::string ScriptObject::getBackend() const
{
	return "CSharp";
}

ScriptBuilder::ScriptBuilder(SharpBackend* pInterop, ScriptObject* pObject)
	: m_pInterop(pInterop), m_pObject(pObject), m_valid(false)
{
	resolveClass();
}

void ScriptBuilder::resolveClass()
{
	// Get the assembly for this class and check if it has been loaded
	Memory::reference<SharpBackend::_Assembly> assembly = m_pInterop->getAssembly(m_pObject->m_assembly);
	if (!assembly.valid())
	{
		return;
	}
	MonoImage* pImage = assembly->pImage;
	if (pImage == nullptr)
	{
		return;
	}

	// Get class object
	m_pObject->m_pClass = mono_class_from_name(pImage, m_pObject->m_nspace.c_str(), m_pObject->m_name.c_str());

	// Check if the class was found or not
	if (m_pObject->m_pClass)
	{
		m_valid = true;
	}

	// Get class virtual table
	m_pObject->m_pVTable = mono_class_vtable(m_pInterop->m_pItDomain, m_pObject->m_pClass);

	// Initialize the object virtual table
	mono_runtime_class_init(m_pObject->m_pVTable);
	
	// Create object
	m_pObject->m_pInstance = mono_object_new(m_pInterop->m_pItDomain, m_pObject->m_pClass);
}

void ScriptBuilder::resolveField(GenericField* pIField)
{
	// Return if the builder is invalid
	if (!m_valid)
	{
		return;
	}

	// Get field
	pIField->m_pField = mono_class_get_field_from_name(m_pObject->m_pClass, pIField->m_name.c_str());

	// Check if static
	// Check if the field is static
	uint32_t flags = mono_field_get_flags(pIField->m_pField);
	pIField->m_static = (flags & MONO_FIELD_ATTR_STATIC) != 0;

	// We have to make sure that the class is initialized if
	// the field is from a static class
	if (pIField->m_static)
	{
		// Initialize class that whose field we are trying to access
		pIField->m_instance.pVTable = m_pObject->m_pVTable;
	}
	else
	{
		pIField->m_instance.pObject = m_pObject->m_pInstance;
	}
}

void ScriptBuilder::resolveMethod(GenericMethod* pIMethod)
{
	// Return if the builder is invalid
	if (!m_valid)
	{
		return;
	}

	// Get the assembly for this class and check if it has been loaded
	Memory::reference<SharpBackend::_Assembly> assembly = m_pInterop->getAssembly(m_pObject->m_assembly);
	if (!assembly.valid())
	{
		return;
	}
	MonoImage* pImage = assembly->pImage;
	if (pImage == nullptr)
	{
		return;
	}

	//Build a method description object
	MonoMethodDesc* methodDesc = mono_method_desc_new(pIMethod->m_signature.c_str(), NULL);

	//Search the method in the image
	pIMethod->m_pMethod = mono_method_desc_search_in_image(methodDesc, pImage);

	// Free memory
	mono_method_desc_free(methodDesc);

	// Check if the class was created correctly
	if (!pIMethod->m_pMethod)
	{
		return;
	}

	// Check if static
	
	// Check if the field is static
	uint32_t flags = mono_method_get_flags(pIMethod->m_pMethod, nullptr);
	bool is_static = (flags & MONO_METHOD_ATTR_STATIC) != 0;

	// Set object instance
	if (is_static)
	{
		pIMethod->m_pObject = nullptr;
	}
	else
	{
		// Initialize class that whose field we are trying to access
		pIMethod->m_pObject = this->m_pObject->m_pInstance;
		pIMethod->m_pMethod = mono_object_get_virtual_method(pIMethod->m_pObject, pIMethod->m_pMethod);
	}
}

void ScriptBuilder::resolveProperty(GenericProperty* pIProperty)
{
	// Return if the builder is invalid
	if (!m_valid)
	{
		return;
	}

	// Get the assembly for this class and check if it has been loaded
	Memory::reference<SharpBackend::_Assembly> assembly = m_pInterop->getAssembly(m_pObject->m_assembly);
	if (!assembly.valid())
	{
		return;
	}
	MonoImage* pImage = assembly->pImage;
	if (pImage == nullptr)
	{
		return;
	}

	// Get field
	pIProperty->m_pProperty = mono_class_get_property_from_name(m_pObject->m_pClass, pIProperty->m_name.c_str());

	// Check if static
	MonoMethod* getMethod = mono_property_get_get_method(pIProperty->m_pProperty);
	uint32_t flags = mono_method_get_flags(getMethod, nullptr);
	bool is_static = (flags & MONO_METHOD_ATTR_STATIC) != 0;

	// We have to make sure that the class is initialized if
	// the field is from a static class
	if (is_static)
	{
		// Initialize class that whose field we are trying to access
		pIProperty->m_pObject = nullptr;
	}
	else
	{
		pIProperty->m_pObject = m_pObject->m_pInstance;
	}
}

void ScriptBuilder::linkInternal(const std::string& name, void* lambdaCB)
{
	mono_add_internal_call(name.c_str(), lambdaCB);
}

GenericField::GenericField()
	: m_name(""), m_static(false), m_pField(nullptr), m_instance()
{
}

GenericField::GenericField(const std::string& name)
	: m_name(name), m_static(false), m_pField(nullptr), m_instance()
{
}

GenericField::GenericField(MonoClassField* pField, MonoObject* pObject)
	: m_pField(pField), m_static(false), m_name(mono_field_get_name(pField))
{
	m_instance.pObject = pObject;
}

GenericField::GenericField(MonoClassField* pField, MonoVTable* pVTable)
	: m_pField(pField), m_static(true), m_name(mono_field_get_name(pField))
{
	m_instance.pVTable = pVTable;
}

const std::string& GenericField::getName() const
{
	return m_name;
}

void GenericField::getValue(void** pValue)
{
	MonoObject* pObject = {};

	// Check if field is static and set value accordingly
	if (m_static)
	{
		pObject = mono_field_get_value_object(mono_vtable_domain(m_instance.pVTable), m_pField, m_instance.pObject);
	}
	else
	{
		pObject = mono_field_get_value_object(mono_object_get_domain(m_instance.pObject), m_pField, m_instance.pObject);
	}

	*pValue = pObject;
}

void GenericField::setValue(void* pValue)
{
	// Check if field is static and set value accordingly
	if (m_static)
	{
		mono_field_static_set_value(m_instance.pVTable, m_pField, pValue);
	}
	else
	{
		mono_field_set_value(m_instance.pObject, m_pField, pValue);
	}
}

GenericMethod::GenericMethod()
{
}

GenericMethod::GenericMethod(const std::string& signature)
	: m_signature(signature)
{
}

GenericMethod::GenericMethod(MonoMethod* pMethod, MonoObject* pObject)
	: m_pMethod(pMethod), m_pObject(pObject), m_signature(mono_method_get_name(m_pMethod))
{
}

const std::string& GenericMethod::getSignature() const
{
	return m_signature;
}

void* GenericMethod::invokeGeneric(void** params, Exception*& ex)
{
	// Store exception here
	MonoObject* exception;

	// Invoke the method
	MonoObject* ret = mono_runtime_invoke(m_pMethod, m_pObject, params, &exception);

	// Check if an exception was raised
	if (exception)
	{
		// Reset return object
		ret = nullptr;

		// Create exception
		ex = new Exception(exception);
	}
	return ret;
}

SharpBackend::SharpBackend()
	: m_pItDomain(nullptr)
{
	// Set directories where mono libraries are at
	// By default using premake they will be copied to 
	// lib and etc folders in the output folder
	mono_set_dirs("mono/lib", "mono/etc");


	// Load the default Mono configuration file, this is needed
	// if you are planning on using the dll maps defined on the
	// system configuration
	mono_config_parse(NULL);

#ifdef XLANG_MACRO_DEBUG
	// clang-format off
	const char* options[] =
	{
		"--soft-breakpoints",
		"--debugger-agent=transport=dt_socket,suspend=n,server=y,address=127.0.0.1:55555,embedding=1",
		"--debug-domain-unload",

		// GC options:
		// check-remset-consistency: Makes sure that write barriers are properly issued in native code,
		// and therefore
		//    all old->new generation references are properly present in the remset. This is easy to mess
		//    up in native code by performing a simple memory copy without a barrier, so it's important to
		//    keep the option on.
		// verify-before-collections: Unusure what exactly it does, but it sounds like it could help track
		// down
		//    things like accessing released/moved objects, or attempting to release handles for an
		//    unloaded domain.
		// xdomain-checks: Makes sure that no references are left when a domain is unloaded.
		"--gc-debug=check-remset-consistency,verify-before-collections,xdomain-checks"
	};
	// clang-format on
	mono_jit_parse_options(sizeof(options) / sizeof(char*), const_cast<char**>(options));
	mono_debug_init(MONO_DEBUG_FORMAT_MONO);
#endif

	// Create the mono domain
	m_pJITDomain = mono_jit_init_version("MonoManager", "v4.0.30319");
}

SharpBackend::~SharpBackend()
{
	// Unload assemblies
	unload();

	// Clean up the domain
	shutdown();
}

void SharpBackend::load(const std::string& assemblyPath)
{
	if (m_pItDomain)
	{
		// Get the root and set it as the main domain
		MonoDomain* root = mono_get_root_domain();

		if (mono_domain_set(root, 0))
		{
			// Unload domain
			mono_domain_unload(m_pItDomain);

			// Clean all objects
			mono_gc_collect(mono_gc_max_generation());

			// Unload all assemblies
			for (const Memory::reference<_Assembly>& assembly : m_loadedAssemblies)
			{
				mono_image_close(assembly->pImage);
			}

			// Clear all loaded assemblies
			m_loadedAssemblies.clear();
		}
	}

	// Create domain
	m_pItDomain = mono_domain_create_appdomain(const_cast<char*>("appdomain"), nullptr);

	// Set the domain
	if (mono_domain_set(m_pItDomain, 0))
	{
		mono_thread_attach(m_pItDomain);
	}

	// Create new assembly entry
	Memory::reference<_Assembly> assembly = new _Assembly();
	assembly->Path = assemblyPath;

	// Load the assembly

	// Open the file and get the last position
	std::ifstream ifs(assemblyPath, std::ios::binary | std::ios::ate);
	std::ifstream::pos_type pos = ifs.tellg();

	// Initialize vector buffer
	std::vector<char> assemblyContents(pos);

	// Seek start and copy contents to vector
	ifs.seekg(0, std::ios::beg);
	ifs.read(assemblyContents.data(), pos);

	// Create image
	MonoImageOpenStatus status;
	assembly->pImage = mono_image_open_from_data_with_name(assemblyContents.data(), assemblyContents.size(), 1, &status,
		0, assemblyPath.c_str());

	// Check if loaded correctly
	if (status != MONO_IMAGE_OK || assembly->pImage == nullptr)
	{
		std::cout << "Image can't be created '" << assemblyPath << "'\n";
		return;
	}

	// load the assembly
	assembly->pAssembly = mono_assembly_load_from_full(assembly->pImage, assemblyPath.c_str(), &status, false);
	if (status != MONO_IMAGE_OK || assembly->pAssembly == nullptr)
	{
		mono_image_close(assembly->pImage);
		std::cout << "Assembly can't be loaded '" << assemblyPath << "'\n";
		return;
	}

	// Get name of assembly
	assembly->Name = mono_assembly_name_get_name(mono_assembly_get_name(assembly->pAssembly));

	m_loadedAssemblies.push_back(assembly);
}

void SharpBackend::unload()
{
	// If already unloaded ignore request
	if (m_pItDomain == NULL)
	{
		return;
	}

	if (mono_domain_set(m_pJITDomain, 0))
	{
		// Unload domain
		mono_domain_unload(m_pItDomain);
		m_pItDomain = NULL;

		mono_gc_collect(mono_gc_max_generation());
	}

	for (ScriptObject* object : m_objects)
	{
		delete object;
	}

	for (Memory::reference<_Assembly>& assembly : m_loadedAssemblies)
	{
		// Clean all objects
		mono_image_close(assembly->pImage);
	}
			
	// Clear all loaded assemblies
	// This causes all assemblies to close their images and assemblies
	m_loadedAssemblies.clear();
}

void SharpBackend::shutdown()
{
	// Shutdown
	mono_jit_cleanup(m_pJITDomain);
}

Memory::reference<SharpBackend::_Assembly> SharpBackend::getAssembly(const std::string& name)
{
	for (Memory::reference<_Assembly>& assembly : m_loadedAssemblies)
	{
		if (assembly->Name == name)
		{
			return assembly;
		}
	}
}

IObject* SharpBackend::getObject(const std::string& instanceID)
{
	for (IObject* obj : m_objects)
	{
		if (obj->getInstanceID() == instanceID)
		{
			return obj;
		}
	}

	return nullptr;
}

void SharpBackend::newObject(IObject* pObject)
{
	// Initialize object and add it to the object vector
	ScriptObject* pSharp = static_cast<ScriptObject*>(pObject);
	pSharp->init(this);
	m_objects.push_back(pSharp);
}

std::string SharpBackend::getName()
{
	return "CSharp";
}

bool SharpBackend::init()
{
	return true;
}

Exception::Exception()
{
}

Exception::Exception(MonoObject* ex)
{
	// Get class
	MonoClass* pExClass = mono_object_get_class(ex);

	// Get properties
	MonoProperty* pMsgProp = mono_class_get_property_from_name(pExClass, "Message");
	MonoProperty* pStackTraceProp = mono_class_get_property_from_name(pExClass, "StackTrace");

	// Get values
	MonoObject* pMsg = mono_property_get_value(pMsgProp, ex, nullptr, nullptr);
	MonoObject* pStackTrace = mono_property_get_value(pStackTraceProp, ex, nullptr, nullptr);

	// Get string values
	Message = TypeConversion<std::string>(pMsg).Value;
	StackTrace = TypeConversion<std::string>(pStackTrace).Value;
}

GenericProperty::GenericProperty()
{
}

GenericProperty::GenericProperty(const std::string& name)
	: m_name(name)
{
}

GenericProperty::GenericProperty(MonoProperty* pProperty, MonoObject* pObject)
	: m_pProperty(pProperty), m_pObject(pObject), m_name(mono_property_get_name(pProperty))
{
}

const std::string& GenericProperty::getName() const
{
	return m_name;
}

void GenericProperty::getValue(void** pValue, Exception*& ex)
{
	// Get value
	MonoObject* exception;
	*pValue = mono_property_get_value(m_pProperty, m_pObject, nullptr, &exception);

	// Check for exception
	if (ex)
	{
		*pValue = nullptr;

		// Create exception
		ex = new Exception(exception);
	}
}

void GenericProperty::setValue(void* pValue, Exception*& ex)
{
	// Set value
	MonoObject* exception;
	void* args[] = { pValue };
	mono_property_set_value(m_pProperty, m_pObject, args, &exception);

	// Check for exception
	if (ex)
	{
		// Create exception
		ex = new Exception(exception);
	}
}

SharpSource::SharpSource(std::string pathToAssembly)
	: m_assembly(pathToAssembly)
{
}

std::string SharpSource::getBackend()
{
	return "CSharp";
}

bool SharpSource::load(IBackend* pBackend)
{
	// Cast to sharp back-end
	SharpBackend* pSharp = static_cast<SharpBackend*>(pBackend);

	// TODO: Move loading to the source
	pSharp->load(this->m_assembly);

	return true;
}
