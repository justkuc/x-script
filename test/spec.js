const Application = require('spectron').Application
const assert = require('assert')
const electronPath = require('electron') // Require Electron from the binaries included in node_modules.
const path = require('path')


describe('Application launch', function () {
  this.timeout(10000)

  before(function () {
    this.app = new Application({
      path: electronPath,
      args: [path.join(__dirname, '..')]
    })
    return this.app.start()
  })

//   afterEach(function () {
//     if (this.app && this.app.isRunning()) {
//       return this.app.stop()
//     }
//   })


  // Checks whether the initial window opens
  it('Shows an initial window', function () {
    return this.app.client.getWindowCount().then(function (count) {
      assert.equal(count, 1)
      // Please note that getWindowCount() will return 2 if `dev tools` are opened.
      // assert.equal(count, 2)
    })
  })


  // Selects a folder, checks, whether the correct class is applied
  // to the open folder, selects a file and checks if a new code slate
  // is opened and present in the DOM
  it('Navigates to folder, selects a file to open', async function() {
    this.app.client.click('#explorerTabId_3');
    this.app.client.waitUntilWindowLoaded();
    const isOpen = await this.app.client.$('.symbol folderOpen');
    //assert.ok(isOpen);

    this.app.client.click('#tabId_24');
    this.app.client.waitUntilWindowLoaded();
    const codeSlate = await this.app.client.getHTML('#codeslate_1');
    assert.ok(codeSlate);
  })


  // Clicks on the close file button - checks, whether the code slate
  // correctly disappears from DOM
  it('Closes the opened file - and its code slate', function() {
      this.app.client.$('.closeTabIcon').click();
      const codeSlate = this.app.client.getHTML('#codeslate_1');
      let isExisting = codeSlate.isExisting();
      if (!isExisting){
        assert.ok();
      }
  })
  
  // Closes folder, once again checks, whether the class folderOpen
  // disappears - as it should - once the folder is closed
  it('Closes the opened folder', function() {
    this.app.client.click('#explorerTabId_3');
    const isOpen = this.app.client.$('.symbol folderOpen').isExisting();
    if(!isOpen) {
        assert.ok();
    }
  })


  // Passes key presses (CTRL + N) to the program, to create a new untitled file
  // 
  it('Creates a new file with shortcut - CTRL + N (and code slate)', function() {
      this.app.client.keys(['Control', 'n']);
      const codeSlate = this.app.client.getHTML('#codeslate_2');
      assert.ok(codeSlate);
  })
})
