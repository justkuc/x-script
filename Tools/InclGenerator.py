import argparse
import glob
import re
from pathlib import Path
from shutil import copyfile

# Get commandline arguments
parser = argparse.ArgumentParser(description='Generates C++ Incl file')
parser.add_argument(dest='SrcDir', action="store", type=str, help='Source file directory')

# Parse arguments
args = parser.parse_args()

files = ""
classes = ""
out_dir = args.SrcDir + "cpp/src/Dynamic/"
sources = ""

with open(args.SrcDir + "main", 'r') as file:
    main_class = file.read().replace('\n', '')

Path(out_dir).mkdir(exist_ok = True, parents = True)

# Python scan
for source in glob.glob(args.SrcDir + "Python/*.h"):
	name = Path(source).stem
	copyfile(source, out_dir + name + ".h")
	files += "#include <{0}>\n".format("Dynamic/" + name + ".h")
	
	if name != main_class:
		classes += "\truntime.newObject<{0}>();\n".format(name)
	else:
		classes += "\t{0}* pMain = runtime.newObject<{0}>();\n".format(name)

	sources += "\truntime.addSource<PythonSource>(\"{}\");\n".format(name)

# Python scan
for source in glob.glob(args.SrcDir + "Sharp/*.h"):
	name = Path(source).stem
	copyfile(source, out_dir + name + ".h")
	files += "#include <{0}>\n".format("Dynamic/" + name + ".h")

	if name != main_class:
		classes += "\truntime.newObject<{0}>();\n".format(name)
	else:
		classes += "\t{0}* pMain = runtime.newObject<{0}>();\n".format(name)

	#sources += "\truntime.addSource<SharpSource>(\"{}.dll\");\n".format(name)

incl_template = "#pragma once\n\n// Create runtime\nXLangRuntime runtime;\n\n// Includes\n{0}\n// Sources\nvoid xlang_add_sources()\n{{\n\t// Add code sources\n{2}\truntime.addSource<SharpSource>(\"sharp_glue.dll\");\n}}\n\n// Entry point\nvoid xlang_invoke_main()\n{{\n\t// Link objects\n{1}\n\t// Invoke main\n\tpMain->Main->invoke();\n}}".format(files, classes, sources)

with open(out_dir + "Incl.h", "w") as f:
	f.write(incl_template)

