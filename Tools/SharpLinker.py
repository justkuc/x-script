import argparse
import glob
import re
from pathlib import Path

# Get commandline arguments
parser = argparse.ArgumentParser(description='Links C# source files')
parser.add_argument(dest='SrcDir', action="store", type=str, help='Source file directory')
parser.add_argument(dest='Project', action="store", type=str, help='Project file')

# Parse arguments
args = parser.parse_args()

files = ""
outdir = "../Sources/Sharp/"

for source in glob.glob(args.SrcDir + "*.cs"):
	name = Path(source).stem
	files += "<Compile Include=\"{}\"/>\n".format(outdir + name + ".cs")

with open(args.Project, 'r') as file:
    project_source = file.read()

source = ""
for line in project_source.split('\n'):
	result = re.match("^[ ]*<Compile.*$", line)
	
	if result is None:
		source += line + "\n"

source = source.replace("<!--%%files%%-->", files + "<!--%%files%%-->")

with open(args.Project, 'w') as file:
    file.write(source)
